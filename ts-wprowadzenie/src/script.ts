import { UserStatus } from "./enums/user-status.enum";
import { Acc } from "./models/acc.models";
import { Input } from "./models/input.model";
import { User } from "./models/user.model";

const a: string = 'ghjk';
console.log(a);

const b: number | string = 5; //string | number | boolean
const c: string | number | Boolean = a + b;
const d: any = 5;

const arr: number[] = [1, 5];
const arr2: number[] | string[] = ['4', 'f'] // tylko number lub tylko string w tablicy
const arr3: (number | string)[] = ['4', 5] // dowolnie number lub string w tablicy

function fun1(a: number): number {
    return a;
}

function fun2(a: number, b: number): string {
    return "Wynik " + (a + b);
}

function fun3(status: any): string {
    console.log("Hello");
    // return 'Hello';
    switch (status) {
        case 1:
            return 'one';
        default: // default jest potrzebny bo funkcja musi zwracać string
            return 'other';
    }
}

const wyn: string = fun3(1);
console.log(wyn);

const array: number[] = [ -5, 4, -2, 4, -5 ]

const powerElements = array.reduce((acc: number[], currentElement: number) => {
    if (currentElement < 0) {
        acc.push(currentElement*currentElement);
    } else {
        acc.push(currentElement);
    }
    return acc
}, []);

console.log(powerElements);

const user: User = {
    firstName: 'Jan',
    lastName: 'Kowalski',
    city: 'Gdańsk',
    email: 'aa@a.pl',
    // date: new Date(),
    // age: 20
    status: UserStatus.ACTIVE
}

const users: User[] = [user];

console.log(user.firstName, user.lastName);


const input: Input[] = [ 
    { id: 'abc', name: 'Ala' }, 
    { id: 'def', name: 'Tomek' }, 
    { id: 'ghi', name: 'Jan' } 
];

const convertObjects = input.reduce((acc: object[], element: Input) => {
    acc.push({[element.id]: element});
    return acc;
}, []);

console.log(convertObjects);