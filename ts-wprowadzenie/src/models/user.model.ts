import { UserStatus } from "../enums/user-status.enum";

export interface User {
    firstName: string;
    lastName: string;
    city: string;
    email: string;
    date?: Date; // ? - pole opcjonalne
    age?: number; // ? - pole opcjonalne
    status: UserStatus;
}
